.class public final Landroid/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final alert_dark_frame:I = 0x1080000

.field public static final alert_light_frame:I = 0x1080001

.field public static final arrow_down_float:I = 0x1080002

.field public static final arrow_up_float:I = 0x1080003

.field public static final bottom_bar:I = 0x108009a

.field public static final btn_default:I = 0x1080004

.field public static final btn_default_small:I = 0x1080005

.field public static final btn_dialog:I = 0x1080017

.field public static final btn_dropdown:I = 0x1080006

.field public static final btn_minus:I = 0x1080007

.field public static final btn_plus:I = 0x1080008

.field public static final btn_radio:I = 0x1080009

.field public static final btn_star:I = 0x108000a

.field public static final btn_star_big_off:I = 0x108000b

.field public static final btn_star_big_on:I = 0x108000c

.field public static final button_onoff_indicator_off:I = 0x108000e

.field public static final button_onoff_indicator_on:I = 0x108000d

.field public static final checkbox_off_background:I = 0x108000f

.field public static final checkbox_on_background:I = 0x1080010

.field public static final dark_header:I = 0x10800a5

.field public static final dialog_frame:I = 0x1080011

.field public static final dialog_holo_dark_frame:I = 0x10800b2

.field public static final dialog_holo_light_frame:I = 0x10800b3

.field public static final digital_pen_active:I = 0x10803f2

.field public static final digital_pen_background_side_channel:I = 0x10803f7

.field public static final digital_pen_blocked_mic_red:I = 0x10803f3

.field public static final digital_pen_blocked_mic_yellow:I = 0x10803f4

.field public static final digital_pen_low_battery:I = 0x10803f5

.field public static final digital_pen_positioning_problem:I = 0x10803f6

.field public static final divider_horizontal_bright:I = 0x1080012

.field public static final divider_horizontal_dark:I = 0x1080014

.field public static final divider_horizontal_dim_dark:I = 0x1080015

.field public static final divider_horizontal_textfield:I = 0x1080013

.field public static final edit_text:I = 0x1080016

.field public static final editbox_background:I = 0x1080018

.field public static final editbox_background_normal:I = 0x1080019

.field public static final editbox_dropdown_dark_frame:I = 0x108001a

.field public static final editbox_dropdown_light_frame:I = 0x108001b

.field public static final gallery_thumb:I = 0x108001c

.field public static final ic_btn_speak_now:I = 0x10800a4

.field public static final ic_delete:I = 0x108001d

.field public static final ic_dialog_alert:I = 0x1080027

.field public static final ic_dialog_dialer:I = 0x1080028

.field public static final ic_dialog_email:I = 0x1080029

.field public static final ic_dialog_info:I = 0x108009b

.field public static final ic_dialog_map:I = 0x108002a

.field public static final ic_input_add:I = 0x108002b

.field public static final ic_input_delete:I = 0x108002c

.field public static final ic_input_get:I = 0x108002d

.field public static final ic_lock_idle_alarm:I = 0x108002e

.field public static final ic_lock_idle_charging:I = 0x108001e

.field public static final ic_lock_idle_lock:I = 0x108001f

.field public static final ic_lock_idle_low_battery:I = 0x1080020

.field public static final ic_lock_lock:I = 0x108002f

.field public static final ic_lock_power_off:I = 0x1080030

.field public static final ic_lock_silent_mode:I = 0x1080031

.field public static final ic_lock_silent_mode_off:I = 0x1080032

.field public static final ic_media_ff:I = 0x1080021

.field public static final ic_media_next:I = 0x1080022

.field public static final ic_media_pause:I = 0x1080023

.field public static final ic_media_play:I = 0x1080024

.field public static final ic_media_previous:I = 0x1080025

.field public static final ic_media_rew:I = 0x1080026

.field public static final ic_menu_add:I = 0x1080033

.field public static final ic_menu_agenda:I = 0x1080034

.field public static final ic_menu_always_landscape_portrait:I = 0x1080035

.field public static final ic_menu_call:I = 0x1080036

.field public static final ic_menu_camera:I = 0x1080037

.field public static final ic_menu_close_clear_cancel:I = 0x1080038

.field public static final ic_menu_compass:I = 0x1080039

.field public static final ic_menu_crop:I = 0x108003a

.field public static final ic_menu_day:I = 0x108003b

.field public static final ic_menu_delete:I = 0x108003c

.field public static final ic_menu_directions:I = 0x108003d

.field public static final ic_menu_edit:I = 0x108003e

.field public static final ic_menu_gallery:I = 0x108003f

.field public static final ic_menu_help:I = 0x1080040

.field public static final ic_menu_info_details:I = 0x1080041

.field public static final ic_menu_manage:I = 0x1080042

.field public static final ic_menu_mapmode:I = 0x1080043

.field public static final ic_menu_month:I = 0x1080044

.field public static final ic_menu_more:I = 0x1080045

.field public static final ic_menu_my_calendar:I = 0x1080046

.field public static final ic_menu_mylocation:I = 0x1080047

.field public static final ic_menu_myplaces:I = 0x1080048

.field public static final ic_menu_preferences:I = 0x1080049

.field public static final ic_menu_recent_history:I = 0x108004a

.field public static final ic_menu_report_image:I = 0x108004b

.field public static final ic_menu_revert:I = 0x108004c

.field public static final ic_menu_rotate:I = 0x108004d

.field public static final ic_menu_save:I = 0x108004e

.field public static final ic_menu_search:I = 0x108004f

.field public static final ic_menu_send:I = 0x1080050

.field public static final ic_menu_set_as:I = 0x1080051

.field public static final ic_menu_share:I = 0x1080052

.field public static final ic_menu_slideshow:I = 0x1080053

.field public static final ic_menu_sort_alphabetically:I = 0x108009c

.field public static final ic_menu_sort_by_size:I = 0x108009d

.field public static final ic_menu_today:I = 0x1080054

.field public static final ic_menu_upload:I = 0x1080055

.field public static final ic_menu_upload_you_tube:I = 0x1080056

.field public static final ic_menu_view:I = 0x1080057

.field public static final ic_menu_week:I = 0x1080058

.field public static final ic_menu_zoom:I = 0x1080059

.field public static final ic_notification_clear_all:I = 0x108005a

.field public static final ic_notification_overlay:I = 0x108005b

.field public static final ic_partial_secure:I = 0x108005c

.field public static final ic_popup_disk_full:I = 0x108005d

.field public static final ic_popup_reminder:I = 0x108005e

.field public static final ic_popup_sync:I = 0x108005f

.field public static final ic_search_category_default:I = 0x1080060

.field public static final ic_secure:I = 0x1080061

.field public static final list_selector_background:I = 0x1080062

.field public static final menu_frame:I = 0x1080063

.field public static final menu_full_frame:I = 0x1080064

.field public static final menuitem_background:I = 0x1080065

.field public static final op_letter_photo_a0:I = 0x108ff53

.field public static final op_letter_photo_a1:I = 0x108ff54

.field public static final op_letter_photo_a2:I = 0x108ff55

.field public static final op_letter_photo_a3:I = 0x108ff56

.field public static final op_letter_photo_a4:I = 0x108ff57

.field public static final op_letter_photo_a5:I = 0x108ff58

.field public static final op_letter_photo_a6:I = 0x108ff59

.field public static final op_letter_photo_a7:I = 0x108ff60

.field public static final op_letter_photo_b0:I = 0x108ff61

.field public static final op_letter_photo_b1:I = 0x108ff62

.field public static final op_letter_photo_b2:I = 0x108ff63

.field public static final op_letter_photo_b3:I = 0x108ff64

.field public static final op_letter_photo_b4:I = 0x108ff65

.field public static final op_letter_photo_b5:I = 0x108ff66

.field public static final op_letter_photo_b6:I = 0x108ff67

.field public static final op_letter_photo_b7:I = 0x108ff68

.field public static final op_letter_photo_c0:I = 0x108ff69

.field public static final op_letter_photo_c1:I = 0x108ff6a

.field public static final op_letter_photo_c2:I = 0x108ff6b

.field public static final op_letter_photo_c3:I = 0x108ff6c

.field public static final op_letter_photo_c4:I = 0x108ff6d

.field public static final op_letter_photo_c5:I = 0x108ff6e

.field public static final op_letter_photo_c6:I = 0x108ff6f

.field public static final op_letter_photo_c7:I = 0x108ff70

.field public static final op_letter_photo_d0:I = 0x108ff71

.field public static final op_letter_photo_d1:I = 0x108ff72

.field public static final op_letter_photo_d2:I = 0x108ff73

.field public static final op_letter_photo_d3:I = 0x108ff74

.field public static final op_letter_photo_d4:I = 0x108ff75

.field public static final op_letter_photo_d5:I = 0x108ff76

.field public static final op_letter_photo_d6:I = 0x108ff77

.field public static final op_letter_photo_d7:I = 0x108ff78

.field public static final op_letter_photo_e0:I = 0x108ff79

.field public static final op_letter_photo_e1:I = 0x108ff7a

.field public static final op_letter_photo_e2:I = 0x108ff7b

.field public static final op_letter_photo_e3:I = 0x108ff7c

.field public static final op_letter_photo_e4:I = 0x108ff7d

.field public static final op_letter_photo_e5:I = 0x108ff7e

.field public static final op_letter_photo_e6:I = 0x108ff7f

.field public static final op_letter_photo_e7:I = 0x108ff80

.field public static final op_letter_photo_f0:I = 0x108ff81

.field public static final op_letter_photo_f1:I = 0x108ff82

.field public static final op_letter_photo_f2:I = 0x108ff83

.field public static final op_letter_photo_f3:I = 0x108ff84

.field public static final op_letter_photo_f4:I = 0x108ff85

.field public static final op_letter_photo_f5:I = 0x108ff86

.field public static final op_letter_photo_f6:I = 0x108ff87

.field public static final op_letter_photo_f7:I = 0x108ff88

.field public static final op_letter_photo_g0:I = 0x108ff89

.field public static final op_letter_photo_g1:I = 0x108ff8a

.field public static final op_letter_photo_g2:I = 0x108ff8b

.field public static final op_letter_photo_g3:I = 0x108ff8c

.field public static final op_letter_photo_g4:I = 0x108ff8d

.field public static final op_letter_photo_g5:I = 0x108ff8f

.field public static final op_letter_photo_g6:I = 0x108ff90

.field public static final op_letter_photo_g7:I = 0x108ff91

.field public static final op_letter_photo_h0:I = 0x108ff92

.field public static final op_letter_photo_h1:I = 0x108ff93

.field public static final op_letter_photo_h2:I = 0x108ff94

.field public static final op_letter_photo_h3:I = 0x108ff95

.field public static final op_letter_photo_h4:I = 0x108ff96

.field public static final op_letter_photo_h5:I = 0x108ff97

.field public static final op_letter_photo_h6:I = 0x108ff98

.field public static final op_letter_photo_h7:I = 0x108ff99

.field public static final op_letter_photo_i0:I = 0x108ff9a

.field public static final op_letter_photo_i1:I = 0x108ff9b

.field public static final op_letter_photo_i2:I = 0x108ff9c

.field public static final op_letter_photo_i3:I = 0x108ff9d

.field public static final op_letter_photo_i4:I = 0x108ff9e

.field public static final op_letter_photo_i5:I = 0x108ff9f

.field public static final op_letter_photo_i6:I = 0x108ffa0

.field public static final op_letter_photo_i7:I = 0x108ffa1

.field public static final op_letter_photo_j0:I = 0x108ffa2

.field public static final op_letter_photo_j1:I = 0x108ffa3

.field public static final op_letter_photo_j2:I = 0x108ffa4

.field public static final op_letter_photo_j3:I = 0x108ffa5

.field public static final op_letter_photo_j4:I = 0x108ffa6

.field public static final op_letter_photo_j5:I = 0x108ffa7

.field public static final op_letter_photo_j6:I = 0x108ffa8

.field public static final op_letter_photo_j7:I = 0x108ffa9

.field public static final op_letter_photo_k0:I = 0x108ffaa

.field public static final op_letter_photo_k1:I = 0x108ffab

.field public static final op_letter_photo_k2:I = 0x108ffac

.field public static final op_letter_photo_k3:I = 0x108ffad

.field public static final op_letter_photo_k4:I = 0x108ffae

.field public static final op_letter_photo_k5:I = 0x108ffaf

.field public static final op_letter_photo_k6:I = 0x108ffb0

.field public static final op_letter_photo_k7:I = 0x108ffb1

.field public static final op_letter_photo_l0:I = 0x108ffb2

.field public static final op_letter_photo_l1:I = 0x108ffb3

.field public static final op_letter_photo_l2:I = 0x108ffb4

.field public static final op_letter_photo_l3:I = 0x108ffb5

.field public static final op_letter_photo_l4:I = 0x108ffb6

.field public static final op_letter_photo_l5:I = 0x108ffb7

.field public static final op_letter_photo_l6:I = 0x108ffb8

.field public static final op_letter_photo_l7:I = 0x108ffb9

.field public static final op_letter_photo_m0:I = 0x108ffba

.field public static final op_letter_photo_m1:I = 0x108ffbb

.field public static final op_letter_photo_m2:I = 0x108ffbc

.field public static final op_letter_photo_m3:I = 0x108ffbd

.field public static final op_letter_photo_m4:I = 0x108ffbe

.field public static final op_letter_photo_m5:I = 0x108ffbf

.field public static final op_letter_photo_m6:I = 0x108ffc0

.field public static final op_letter_photo_m7:I = 0x108ffc1

.field public static final op_letter_photo_n0:I = 0x108ffc2

.field public static final op_letter_photo_n1:I = 0x108ffc3

.field public static final op_letter_photo_n2:I = 0x108ffc4

.field public static final op_letter_photo_n3:I = 0x108ffc5

.field public static final op_letter_photo_n4:I = 0x108ffc6

.field public static final op_letter_photo_n5:I = 0x108ffc7

.field public static final op_letter_photo_n6:I = 0x108ffc8

.field public static final op_letter_photo_n7:I = 0x108ffc9

.field public static final op_letter_photo_o0:I = 0x108ffca

.field public static final op_letter_photo_o1:I = 0x108ffcb

.field public static final op_letter_photo_o2:I = 0x108ffcc

.field public static final op_letter_photo_o3:I = 0x108ffcd

.field public static final op_letter_photo_o4:I = 0x108ffce

.field public static final op_letter_photo_o5:I = 0x108ffcf

.field public static final op_letter_photo_o6:I = 0x108ffd0

.field public static final op_letter_photo_o7:I = 0x108ffd1

.field public static final op_letter_photo_p0:I = 0x108ffd2

.field public static final op_letter_photo_p1:I = 0x108ffd3

.field public static final op_letter_photo_p2:I = 0x108ffd4

.field public static final op_letter_photo_p3:I = 0x108ffd5

.field public static final op_letter_photo_p4:I = 0x108ffd6

.field public static final op_letter_photo_p5:I = 0x108ffd7

.field public static final op_letter_photo_p6:I = 0x108ffd8

.field public static final op_letter_photo_p7:I = 0x108ffd9

.field public static final op_letter_photo_q0:I = 0x108ffda

.field public static final op_letter_photo_q1:I = 0x108ffdb

.field public static final op_letter_photo_q2:I = 0x108ffdc

.field public static final op_letter_photo_q3:I = 0x108ffde

.field public static final op_letter_photo_q4:I = 0x108ffdf

.field public static final op_letter_photo_q5:I = 0x108ffe0

.field public static final op_letter_photo_q6:I = 0x108ffe1

.field public static final op_letter_photo_q7:I = 0x108ffe2

.field public static final op_letter_photo_r0:I = 0x108ffe3

.field public static final op_letter_photo_r1:I = 0x108ffe4

.field public static final op_letter_photo_r2:I = 0x108ffe5

.field public static final op_letter_photo_r3:I = 0x108ffe6

.field public static final op_letter_photo_r4:I = 0x108ffe7

.field public static final op_letter_photo_r5:I = 0x108ffe8

.field public static final op_letter_photo_r6:I = 0x108ffe9

.field public static final op_letter_photo_r7:I = 0x108ffea

.field public static final op_letter_photo_s0:I = 0x108ffeb

.field public static final op_letter_photo_s1:I = 0x108ffec

.field public static final op_letter_photo_s2:I = 0x108ffed

.field public static final op_letter_photo_s3:I = 0x108ffee

.field public static final op_letter_photo_s4:I = 0x108ffef

.field public static final op_letter_photo_s5:I = 0x108fff0

.field public static final op_letter_photo_s6:I = 0x108fff1

.field public static final op_letter_photo_s7:I = 0x108fff2

.field public static final op_letter_photo_t0:I = 0x108fff3

.field public static final op_letter_photo_t1:I = 0x108fff4

.field public static final op_letter_photo_t2:I = 0x108fff5

.field public static final op_letter_photo_t3:I = 0x108fff6

.field public static final op_letter_photo_t4:I = 0x108fff7

.field public static final op_letter_photo_t5:I = 0x108fff8

.field public static final op_letter_photo_t6:I = 0x108fff9

.field public static final op_letter_photo_t7:I = 0x108fffa

.field public static final op_letter_photo_u0:I = 0x108fffb

.field public static final op_letter_photo_u1:I = 0x108fffc

.field public static final op_letter_photo_u2:I = 0x108fffd

.field public static final op_letter_photo_u3:I = 0x108fffe

.field public static final op_letter_photo_u4:I = 0x108ff4f

.field public static final op_letter_photo_u5:I = 0x108ff4e

.field public static final op_letter_photo_u6:I = 0x108ff4d

.field public static final op_letter_photo_u7:I = 0x108ff4c

.field public static final op_letter_photo_v0:I = 0x108ff4b

.field public static final op_letter_photo_v1:I = 0x108ff4a

.field public static final op_letter_photo_v2:I = 0x108ff49

.field public static final op_letter_photo_v3:I = 0x108ff48

.field public static final op_letter_photo_v4:I = 0x108ff47

.field public static final op_letter_photo_v5:I = 0x108ff46

.field public static final op_letter_photo_v6:I = 0x108ff45

.field public static final op_letter_photo_v7:I = 0x108ff44

.field public static final op_letter_photo_w0:I = 0x108ff43

.field public static final op_letter_photo_w1:I = 0x108ff42

.field public static final op_letter_photo_w2:I = 0x108ff41

.field public static final op_letter_photo_w3:I = 0x108ff40

.field public static final op_letter_photo_w4:I = 0x108ff3f

.field public static final op_letter_photo_w5:I = 0x108ff3e

.field public static final op_letter_photo_w6:I = 0x108ff3d

.field public static final op_letter_photo_w7:I = 0x108ff3c

.field public static final op_letter_photo_x0:I = 0x108ff3b

.field public static final op_letter_photo_x1:I = 0x108ff3a

.field public static final op_letter_photo_x2:I = 0x108ff39

.field public static final op_letter_photo_x3:I = 0x108ff38

.field public static final op_letter_photo_x4:I = 0x108ff37

.field public static final op_letter_photo_x5:I = 0x108ff36

.field public static final op_letter_photo_x6:I = 0x108ff35

.field public static final op_letter_photo_x7:I = 0x108ff34

.field public static final op_letter_photo_y0:I = 0x108ff33

.field public static final op_letter_photo_y1:I = 0x108ff32

.field public static final op_letter_photo_y2:I = 0x108ff31

.field public static final op_letter_photo_y3:I = 0x108ff30

.field public static final op_letter_photo_y4:I = 0x108ff2f

.field public static final op_letter_photo_y5:I = 0x108ff2e

.field public static final op_letter_photo_y6:I = 0x108ff2d

.field public static final op_letter_photo_y7:I = 0x108ff2c

.field public static final op_letter_photo_z0:I = 0x108ff2b

.field public static final op_letter_photo_z1:I = 0x108ff2a

.field public static final op_letter_photo_z2:I = 0x108ff29

.field public static final op_letter_photo_z3:I = 0x108ff28

.field public static final op_letter_photo_z4:I = 0x108ff27

.field public static final op_letter_photo_z5:I = 0x108ff26

.field public static final op_letter_photo_z6:I = 0x108ff25

.field public static final op_letter_photo_z7:I = 0x108ff24

.field public static final op_me_photo:I = 0x108ff50

.field public static final op_others_photo:I = 0x108ff51

.field public static final op_stranger_photo:I = 0x108ff52

.field public static final picture_frame:I = 0x1080066

.field public static final presence_audio_away:I = 0x10800af

.field public static final presence_audio_busy:I = 0x10800b0

.field public static final presence_audio_online:I = 0x10800b1

.field public static final presence_away:I = 0x1080067

.field public static final presence_busy:I = 0x1080068

.field public static final presence_invisible:I = 0x1080069

.field public static final presence_offline:I = 0x108006a

.field public static final presence_online:I = 0x108006b

.field public static final presence_video_away:I = 0x10800ac

.field public static final presence_video_busy:I = 0x10800ad

.field public static final presence_video_online:I = 0x10800ae

.field public static final progress_horizontal:I = 0x108006c

.field public static final progress_indeterminate_horizontal:I = 0x108006d

.field public static final radiobutton_off_background:I = 0x108006e

.field public static final radiobutton_on_background:I = 0x108006f

.field public static final screen_background_dark:I = 0x1080098

.field public static final screen_background_dark_transparent:I = 0x10800a9

.field public static final screen_background_light:I = 0x1080099

.field public static final screen_background_light_transparent:I = 0x10800aa

.field public static final spinner_background:I = 0x1080070

.field public static final spinner_dropdown_background:I = 0x1080071

.field public static final star_big_off:I = 0x1080073

.field public static final star_big_on:I = 0x1080072

.field public static final star_off:I = 0x1080075

.field public static final star_on:I = 0x1080074

.field public static final stat_notify_call_mute:I = 0x1080076

.field public static final stat_notify_chat:I = 0x1080077

.field public static final stat_notify_error:I = 0x1080078

.field public static final stat_notify_missed_call:I = 0x108007f

.field public static final stat_notify_more:I = 0x1080079

.field public static final stat_notify_sdcard:I = 0x108007a

.field public static final stat_notify_sdcard_prepare:I = 0x10800ab

.field public static final stat_notify_sdcard_usb:I = 0x108007b

.field public static final stat_notify_sync:I = 0x108007c

.field public static final stat_notify_sync_noanim:I = 0x108007d

.field public static final stat_notify_voicemail:I = 0x108007e

.field public static final stat_sys_data_bluetooth:I = 0x1080080

.field public static final stat_sys_download:I = 0x1080081

.field public static final stat_sys_download_done:I = 0x1080082

.field public static final stat_sys_headset:I = 0x1080083

.field public static final stat_sys_phone_call:I = 0x1080084
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_phone_call_forward:I = 0x1080085
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_phone_call_on_hold:I = 0x1080086
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_speakerphone:I = 0x1080087

.field public static final stat_sys_upload:I = 0x1080088

.field public static final stat_sys_upload_done:I = 0x1080089

.field public static final stat_sys_vp_phone_call:I = 0x10800a7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_vp_phone_call_on_hold:I = 0x10800a8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_warning:I = 0x108008a

.field public static final status_bar_item_app_background:I = 0x108008b

.field public static final status_bar_item_background:I = 0x108008c

.field public static final sym_action_call:I = 0x108008d

.field public static final sym_action_chat:I = 0x108008e

.field public static final sym_action_email:I = 0x108008f

.field public static final sym_call_incoming:I = 0x1080090

.field public static final sym_call_missed:I = 0x1080091

.field public static final sym_call_outgoing:I = 0x1080092

.field public static final sym_contact_card:I = 0x1080094

.field public static final sym_def_app_icon:I = 0x1080093

.field public static final title_bar:I = 0x1080095

.field public static final title_bar_tall:I = 0x10800a6

.field public static final toast_frame:I = 0x1080096

.field public static final zoom_plate:I = 0x1080097


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
