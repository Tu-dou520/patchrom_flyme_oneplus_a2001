.class public final Lcom/hydrongen/internal/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/hydrongen/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final actionbar_background_color_dark:I = 0x305004d

.field public static final edittext_color:I = 0x3050026

.field public static final edittext_color_dark:I = 0x305002b

.field public static final edittext_error_color:I = 0x3050027

.field public static final edittext_error_color_dark:I = 0x305002c

.field public static final edittext_hint_color:I = 0x3050025

.field public static final edittext_hint_color_dark:I = 0x305002a

.field public static final material_white_grey_1050:I = 0x3050078

.field public static final oneplus_orange:I = 0x305006a

.field public static final uc_accent_material_dark:I = 0x3050073

.field public static final uc_accent_material_light:I = 0x3050071

.field public static final uc_link_text_material_dark:I = 0x3050076

.field public static final uc_link_text_material_light:I = 0x3050075

.field public static final uc_list_btn_focused:I = 0x305006c

.field public static final uc_list_btn_normal:I = 0x305006d

.field public static final uc_list_btn_pressed:I = 0x305006b

.field public static final uc_list_popup_bg_color:I = 0x305006e

.field public static final uc_list_popup_text_color:I = 0x3050070

.field public static final uc_list_separator_color:I = 0x305006f

.field public static final uc_material_deep_teal_200:I = 0x3050074

.field public static final uc_material_deep_teal_500:I = 0x3050072

.field public static final uc_seekbar_color:I = 0x3050077

.field public static final uc_seekbar_material_light:I = 0x3050084

.field public static final uc_switch_thumb_material_light:I = 0x3050085


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
